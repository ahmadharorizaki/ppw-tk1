from django import forms
from .models import Pelanggan

class Form_Nama(forms.ModelForm):
    class Meta:
        model = Pelanggan
        fields = ['name']