from main.models import Pelanggan
from django.contrib import admin

# Register your models here.

@admin.register(Pelanggan)
class FastfoodAdmin(admin.ModelAdmin):
	list_display = ('name',)