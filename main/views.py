from main.models import Pelanggan
from Grocery.models import grocery
from recipes.models import recipe
from fastfood.models import fastfood2
from django.forms.forms import Form
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils import http
from .forms import Form_Nama


def home(request):
    form = Form_Nama(request.POST or None)
    if form.is_valid():
        form.save()
    nama_pelanggan = Pelanggan.objects.last()
    recipes = recipe.objects.reverse()[:4]
    groceries = grocery.objects.reverse()[:4]
    fastfoods = fastfood2.objects.reverse()[:4]
    context = {
        'name' : nama_pelanggan.get_name,
        'recipes' : recipes,
        'groceries' : groceries,
        'fastfoods' : fastfoods
    }
    return render(request, 'main/home.html', context)

def landing_page(request):
    form = Form_Nama(request.POST or None)
    context = {
        'form_input_user' : form,
    }
    return render(request, "landingpage.html", context)