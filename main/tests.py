from os import name
from main.models import Pelanggan
from django.http import response
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing_page, home

class TestMain(TestCase):
    def test_apakah_terdapat_url_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_template_landing_page(self):
        response = Client().get("")
        self.assertTemplateUsed(response, 'landingpage.html')
    
    def test_apakah_terdapat_fungsi_pada_views_untuk_landing_page(self):
        found = resolve("/")
        self.assertEqual(found.func, landing_page)

    def test_apakah_terdapat_form_nama_pada_landing_page(self):
        response = Client().get("")
        html_response = response.content.decode('utf8')
        self.assertIn("Name", html_response)
    
    def test_apakah_terdapat_url_home(self):
        response = Client().post('/home/', {'name' : 'Orii'})
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_terdapat_template_home(self):
        response = Client().post('/home/', {'name' : 'Orii'})
        self.assertTemplateUsed(response, 'main/home.html')
    
    def test_apakah_terdapat_fungsi_pada_views_untuk_home(self):
        found = resolve("/home/")
        self.assertEqual(found.func, home)

    def test_model_Pelanggan(self):
        new_pelanggan = Pelanggan.objects.create(name="Ori")
        counting_pelanggan = Pelanggan.objects.all().count()
        self.assertEqual(counting_pelanggan, 1)
    
    def test_apakah_form_bisa_menyimpan_data(self):
        response = Client().post('/home/', {'name' : 'Orii'})
        counting_pelanggan = Pelanggan.objects.all().count()
        self.assertEqual(counting_pelanggan, 1)