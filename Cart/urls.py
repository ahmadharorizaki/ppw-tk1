from django.contrib import admin
from django.urls import include, path
from .views import cart

urlpatterns = [
    path('', cart, name="cart"),
]