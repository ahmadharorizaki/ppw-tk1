from django.db import models

# Create your models here.
class Cart(models.Model):
    nama_cart = models.CharField(max_length = 256)
    location_cart = models.CharField(max_length = 1000, null = True)
    harga_cart = models.PositiveIntegerField()
    foto_cart = models.ImageField(null=True, blank=True)
    porsi = models.IntegerField()

    
    def __str__(self):
        return self.name