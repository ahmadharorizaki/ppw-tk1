from django.shortcuts import render
from .models import fastfood2, fastfood
from cloudinary.forms import cl_init_js_callbacks

# Create your views here.
def getfastfood(request):
    result = fastfood2.objects.all()
    context = {'result':result}
    return render(request, 'fastfood.html',context)

def tocart(request,id):
    desc = fastfood2.objects.get(id=id)
    context = {'desc':desc}
    return render(request, 'cart.html',context)

def search(request):
    if request.method =="POST" and request.POST.get("carinama") !=None:
        search = request.POST.get("carinama")
        produk = fastfood2.objects.filter(nama__contains=search)
        response = {
            'produk' : produk,
        }
        return render(request,'search_fastfood.html', response)
    else:
        produk = fastfood2.objects.all()
        response = {
            'produk' : produk,
        }
        return render(request,'search_fastfood.html', response)