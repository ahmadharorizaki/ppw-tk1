from django.contrib import admin
from .models import fastfood, fastfood2

# Register your models here.
@admin.register(fastfood)
class FastfoodAdmin(admin.ModelAdmin):
	list_display = ('nama', 'porsi', 'location', 'harga')

@admin.register(fastfood2)
class FastfoodAdmin(admin.ModelAdmin):
	list_display = ('nama', 'porsi', 'location', 'harga', 'foto')