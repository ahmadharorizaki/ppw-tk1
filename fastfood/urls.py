from django.contrib import admin
from . import views
from django.urls import  path
# # from django.contrib.staticfiles.urls import static
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# # from ..tk1 import settings
 
# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = [
    path('', views.getfastfood, name = 'fastfood'),
    path('cart/<str:id>/', views.tocart, name='cart'),
    path('search/', views.search, name='search'),
]