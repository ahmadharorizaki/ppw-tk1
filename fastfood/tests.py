from fastfood.views import *
from django.urls.base import resolve
from fastfood.models import fastfood2
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import Client
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = webdriver.chrome.options.Options()
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

class MainTestCase(TestCase):

    def test_apakah_url_ada_dan_menggunakan_template(self):
        response = Client().get('/fastfood/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'base.html')

        fastfood2.objects.create(nama ="Kale Salad" ,foto = "jpg", location ="Depok", porsi = 1, harga = 10000)
        obj1 = fastfood2.objects.get(id=1)
        response = Client().get('/fastfood/cart/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'base.html')
    
    def test_model(self):
        fastfood2.objects.create(nama ="Kale Salad" ,foto = "jpg", location ="Depok", porsi = 1, harga = 10000)
        countfastf = fastfood2.objects.all().count()
        self.assertEquals(countfastf,1)

    def test_view(self):
        response = Client().post('/fastfood/', {'nama': 'Kale Salad', 'foto': 'jpg', 'location': 'Depok'} )
        html_kembali = response.content.decode('utf8')
        self.assertIn('a', html_kembali)
        self.assertIn('b', html_kembali)
        self.assertIn('c', html_kembali)