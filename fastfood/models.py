from django.db import models
from cloudinary.models import CloudinaryField

# Create your models here.
class fastfood(models.Model):
    nama = models.CharField(max_length = 256)
    porsi = models.IntegerField()
    location = models.CharField(max_length = 1000)
    harga = models.PositiveIntegerField()

    def class_name(self):
        return "fastfood"

class fastfood2(models.Model):
    nama = models.CharField(max_length = 256)
    porsi = models.IntegerField()
    location = models.CharField(max_length = 1000)
    harga = models.PositiveIntegerField()
    foto = CloudinaryField(null=True, blank=True)

    def class_name(self):
        return "Fast Food"
