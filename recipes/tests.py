from recipes.views import *
from django.urls.base import resolve
from recipes.models import recipe
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import Client
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_url_exist_and_template(self):
        #response = self.client.get('/')
        #self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = Client().get('/recipes/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/recipes.html')
        self.assertTemplateUsed(response,'base.html')

        response = Client().get('/recipes/tambah/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/tambah.html')
        self.assertTemplateUsed(response,'base.html')

        recipe.objects.create(nama ="a" ,photo = "a", deskripsi ="a")
        obj1 = recipe.objects.get(id=1)
        response = Client().get('/recipes/deskripsi/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/deskripsi.html')
        self.assertTemplateUsed(response,'base.html')

    def test_model(self):
        recipe.objects.create(nama ="a" ,photo = "a", deskripsi ="a")
        hitungrecipe = recipe.objects.all().count()
        self.assertEquals(hitungrecipe,1)

    def test_view(self):
        response = Client().post('/recipes/', {'nama': 'a', 'photo': 'b', 'deskripsi': 'c'} )
        html_kembali = response.content.decode('utf8')
        self.assertIn('a', html_kembali)
        self.assertIn('b', html_kembali)
        self.assertIn('c', html_kembali)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
