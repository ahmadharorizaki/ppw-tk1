import cloudinary
from django.db import models
from django.utils import timezone
from datetime import datetime, date
from cloudinary.models import CloudinaryField

class recipe(models.Model):
    nama = models.CharField(max_length=30)
    photo = CloudinaryField(null=True, blank=True)
    deskripsi = models.TextField()
