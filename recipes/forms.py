from django import forms
from django.forms import ModelForm
from django.forms import fields
from django.forms import widgets
from .models import *

class tambahkan(ModelForm):
    class Meta:
        model = recipe;
        fields = '__all__'
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control'}),
            'photo' : forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.Textarea(attrs={'class': 'form-control'}),
        }