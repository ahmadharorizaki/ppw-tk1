from recipes.models import recipe
from django.contrib import admin

# Register your models here.
@admin.register(recipe)
class recipeAdmin(admin.ModelAdmin):
	list_display = ('nama', 'photo', 'deskripsi')