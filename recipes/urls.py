from django.urls import path

from . import views

app_name = 'recipes'

urlpatterns = [
    path('', views.recipes, name='recipes'),
    path('deskripsi/<str:id>/', views.deskripsi, name='deskripsi'),
    path('tambah/', views.tambah, name='tambah'),
]
