from .forms import tambahkan
from django.shortcuts import redirect, render
from .models import recipe
from cloudinary.forms import cl_init_js_callbacks

def recipes(request):
    result = recipe.objects.all()
    context = {'result':result}
    return render(request, 'main/recipes.html',context)

def deskripsi(request,id):
    desc = recipe.objects.get(id=id)
    context = {'desc':desc}
    return render(request, 'main/deskripsi.html',context)

def tambah(request):
    form = tambahkan()
    context = {'form':form}
    if request.method == 'POST':
        form = tambahkan(request.POST, request.FILES)
        form_class = tambahkan
        if form.is_valid():
            form.save()
            return redirect('/recipes')
    return render(request, 'main/tambah.html',context)
