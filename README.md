# Kelompok A07 PPW 2020
Anggota :
Ahmad Harori Zaki Ichsan - 1906353965 (0 dan 1)
Alifah Azka Nisrina - 1906353662 (4)
Fadhl - 1906353952 (2)
Rania Devina Nandini - 1906353731 (3 dan 5)
Link Heroku : menyusul
Aplikasi yang akan kami buat bernama Fresh House. Aplikasi ini bertujuan untuk menyediakan makanan sehat bagi masyarakat di tengah Pandemi COVID-19.
Daftar fitur yang akan kami implementasikan adalah :


Landing page akan meminta nama input user


Home :
a. recommended for you : akan menampilkan grocery dan fast food dengan nama yang menyerupai dengan history pembelian user
b. best seller: akan menampilkan grocery dan fast food dengan pembelian paling banyak
c. new entry : akan menampilan grocery dan fast food yang terbaru dimasukkan oleh developer ke dalam website
d. about us : akan menampilkan company profile kelompok kami
e. contact : akan menjurus pada sosial media kami


Recipe : berisi resep - resep makanan sehat yang bisa dijadikan referensi bahan masakan oleh masyarakat


Grocery : berisi daftar bahan makanan sehat yang bisa dibeli oleh masyarakat, seperti ayam mentah, daging giling, dan bahan masakan lainnya.


Fast Food : berisi makanan cepat saji yang sehat dan bisa dibeli oleh masyarakat


Cart : berisi grocery dan fast food yang dipilih user untuk dibeli


Status pipeline kami untuk Tahap 1 :
Functional Test : Passed
Unit Test : Passed
Coverage : Passed
Deployment : Passed

...