from django.contrib import admin
from . import views
from django.urls import  path

urlpatterns = [
    path('', views.getgrocery, name = 'grocery'),
    path('cart/<str:id>/', views.gocart, name='cart'),
]