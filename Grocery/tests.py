from Grocery.views import *
from django.urls.base import resolve
from Grocery.models import grocery
from django.test import LiveServerTestCase, TestCase, tag
from django.test.client import Client
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    # TEST URL
    def test_url_exist_and_template(self):
        response = Client().get('/grocery/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'base.html')

        grocery.objects.create(nama ="a" ,foto = "a", location ="a", porsi = 1, harga = 10000)
        obj1 = grocery.objects.get(id=1)
        response = Client().get('/grocery/cart/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'base.html')

    # TEST MODELS
    def test_model(self):
        grocery.objects.create(nama ="a" ,foto = "a", location ="a", porsi = 1, harga = 1000)
        hitunggrocery = grocery.objects.all().count()
        self.assertEquals(hitunggrocery,1)

    # TEST VIEWS
    def test_view(self):
        response = Client().post('/grocery/', {'nama': 'a', 'foto': 'b', 'location': 'c'} )
        html_kembali = response.content.decode('utf8')
        self.assertIn('a', html_kembali)
        self.assertIn('b', html_kembali)
        self.assertIn('c', html_kembali)
        

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
