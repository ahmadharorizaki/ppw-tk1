from django.contrib import admin
from .models import grocery

# Register your models here.
@admin.register(grocery)
class GroceryAdmin(admin.ModelAdmin):
	list_display = ('nama', 'porsi', 'location', 'harga')