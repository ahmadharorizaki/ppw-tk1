from django.shortcuts import render
from .models import grocery
from cloudinary.forms import cl_init_js_callbacks

# Create your views here.
def getgrocery(request):
    result = grocery.objects.all()
    context = {'result':result}
    return render(request, 'grocery.html',context)

def gocart(request,id):
    desc = grocery.objects.get(id=id)
    context = {'desc':desc}
    return render(request, 'cart.html',context)
